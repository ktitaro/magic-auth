package main

import (
	"log"
	"magic-auth/app"
)

func main() {
	config := app.ReadConfig()
	log.Fatal(app.RunAppServer(config))
}
