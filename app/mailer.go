package app

import (
	"fmt"
	"net/smtp"
)

func sendMail(
	host,
	port,
	username,
	password,
	from,
	to,
	subject,
	message string,
) error {
	addr := fmt.Sprintf("%s:%s", host, port)
	auth := smtp.PlainAuth("", username, password, host)
	body := []byte(fmt.Sprintf(
		"To: %s\r\nSubject: %s\r\n\r\n%s\r\n",
		to, subject, message,
	))
	err := smtp.SendMail(addr, auth, from, []string{to}, body)
	return err
}
