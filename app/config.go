package app

import (
	"fmt"
	"os"
	"strconv"
)

var (
	defaultServerHost          = "localhost"
	defaultServerPort          = "8000"
	defaultSmtpHost            = "localhost"
	defaultSmtpPort            = "587"
	defaultSmtpUsername        = "admin"
	defaultSmtpPassword        = "admin"
	defaultEmailAddress        = "no-reply@example.com"
	defaultEmailSubject        = "Your authorization link"
	defaultAuthTokenSecret     = "auth-token-secret"
	defaultAuthTokenAudience   = "auth-token-audience"
	defaultAuthTokenExpireTime = fmt.Sprint(60 * 15)
	defaultUserTokenSecret     = "user-token-secret"
	defaultUserTokenAudience   = "user-token-audience"
	defaultUserTokenExpireTime = fmt.Sprint(60 * 60)
)

type Config struct {
	ServerHost      string
	ServerPort      string
	SmtpHost        string
	SmtpPort        string
	SmtpUsername    string
	SmtpPassword    string
	EmailAddress    string
	EmailSubject    string
	AuthTokenAud    string
	AuthTokenExp    int64
	AuthTokenSecret string
	UserTokenAud    string
	UserTokenExp    int64
	UserTokenSecret string
}

func lookupEnv(key, defaultValue string) string {
	if v, ok := os.LookupEnv(key); ok {
		return v
	}
	return defaultValue
}

func ReadConfig() *Config {
	authTokenExpValue := lookupEnv("AUTH_TOKEN_EXPIRE_TIME", defaultAuthTokenExpireTime)
	authTokenExp, err := strconv.Atoi(authTokenExpValue)
	if err != nil {
		panic(err)
	}

	userTokenExpValue := lookupEnv("USER_TOKEN_EXPIRE_TIME", defaultUserTokenExpireTime)
	userTokenExp, err := strconv.Atoi(userTokenExpValue)
	if err != nil {
		panic(err)
	}

	return &Config{
		ServerHost:      lookupEnv("SERVER_HOST", defaultServerHost),
		ServerPort:      lookupEnv("SERVER_PORT", defaultServerPort),
		SmtpHost:        lookupEnv("SMTP_HOST", defaultSmtpHost),
		SmtpPort:        lookupEnv("SMTP_PORT", defaultSmtpPort),
		SmtpUsername:    lookupEnv("SMTP_USERNAME", defaultSmtpUsername),
		SmtpPassword:    lookupEnv("SMTP_PASSWORD", defaultSmtpPassword),
		EmailAddress:    lookupEnv("EMAIL_ADDRESS", defaultEmailAddress),
		EmailSubject:    lookupEnv("EMAIL_SUBJECT", defaultEmailSubject),
		AuthTokenAud:    lookupEnv("AUTH_TOKEN_AUDIENCE", defaultAuthTokenAudience),
		AuthTokenSecret: lookupEnv("AUTH_TOKEN_SECRET", defaultAuthTokenSecret),
		UserTokenAud:    lookupEnv("USER_TOKEN_AUDIENCE", defaultUserTokenAudience),
		UserTokenSecret: lookupEnv("USER_TOKEN_SECRET", defaultUserTokenSecret),
		AuthTokenExp:    int64(authTokenExp),
		UserTokenExp:    int64(userTokenExp),
	}
}
