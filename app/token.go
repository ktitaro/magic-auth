package app

import (
	"time"

	"github.com/golang-jwt/jwt/v4"
)

type tokenClaims struct {
	jwt.RegisteredClaims
	Indentity string `json:"indentity"`
}

func createToken(indentity, secret, aud string, exp int64) (string, error) {
	claims := tokenClaims{
		Indentity: indentity,
		RegisteredClaims: jwt.RegisteredClaims{
			Audience: []string{aud},
			ExpiresAt: jwt.NewNumericDate(
				time.Now().Add(
					time.Duration(exp) * time.Second,
				),
			),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	signedToken, err := token.SignedString([]byte(secret))
	if err != nil {
		return "", err
	}
	return signedToken, nil
}

func validateToken(tokenString, secret string) (bool, *tokenClaims, error) {
	token, err := jwt.ParseWithClaims(
		tokenString,
		&tokenClaims{},
		func(token *jwt.Token) (interface{}, error) {
			return []byte(secret), nil
		},
	)
	if err != nil {
		return false, nil, err
	}
	claims, ok := token.Claims.(*tokenClaims)
	return ok && token.Valid, claims, nil
}
