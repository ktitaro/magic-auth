package app

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/fiber/v2/middleware/requestid"
)

func RunAppServer(config *Config) error {
	app := fiber.New()

	app.Use(cors.New())
	app.Use(logger.New())
	app.Use(recover.New())
	app.Use(compress.New())
	app.Use(requestid.New())

	controller := newAuthController(config)

	app.Post("/auth", controller.sendMagicLink)
	app.Get("/auth/verify", controller.exchangeAuthToken)
	app.Post("/auth/refresh", controller.refreshUserToken)

	addr := fmt.Sprintf(
		"%s:%s",
		config.ServerHost,
		config.ServerPort,
	)

	err := app.Listen(addr)
	return err
}
