package app

import (
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
)

type authController struct {
	config *Config
}

func newAuthController(config *Config) *authController {
	return &authController{config}
}

type sendMagicLinkPayload struct {
	Email    string `json:"email" form:"email"`
	Identity string `json:"indentity" form:"indentity"`
}

func (controller *authController) sendMagicLink(c *fiber.Ctx) error {
	payload := new(sendMagicLinkPayload)

	err := c.BodyParser(payload)
	if err != nil || payload.Email == "" || payload.Identity == "" {
		return c.SendStatus(fiber.StatusBadRequest)
	}

	token, err := createToken(
		payload.Identity,
		controller.config.AuthTokenSecret,
		controller.config.AuthTokenAud,
		controller.config.AuthTokenExp,
	)
	if err != nil {
		log.Println("Failed to create auth token", err)
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	log.Println("token =", token)

	if err := sendMail(
		controller.config.SmtpHost,
		controller.config.SmtpPort,
		controller.config.SmtpUsername,
		controller.config.SmtpPassword,
		controller.config.EmailAddress,
		payload.Email,
		controller.config.EmailSubject,
		fmt.Sprintf(
			"You auth link http://localhost:8000/auth/verify?token=%s",
			token,
		),
	); err != nil {
		log.Println("Failed to send main", err)
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.SendStatus(fiber.StatusOK)
}

type exchangeAuthTokenPayload struct {
	Token string `json:"token" form:"token"`
}

func (controller *authController) exchangeAuthToken(c *fiber.Ctx) error {
	payload := new(exchangeAuthTokenPayload)

	err := c.QueryParser(payload)
	if err != nil || payload.Token == "" {
		return c.SendStatus(fiber.StatusBadRequest)
	}

	ok, claims, err := validateToken(
		payload.Token,
		controller.config.AuthTokenSecret,
	)
	if err != nil {
		log.Println("Failed to parse auth token", err)
		return c.SendStatus(fiber.StatusInternalServerError)
	} else if !ok {
		log.Println("Auth token is invalid")
		return c.SendStatus(fiber.StatusBadRequest)
	}

	token, err := createToken(
		claims.Indentity,
		controller.config.UserTokenSecret,
		controller.config.UserTokenAud,
		controller.config.UserTokenExp,
	)
	if err != nil {
		log.Println("Failed to create user token", err)
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.JSON(fiber.Map{
		"token": token,
	})
}

type refreshUserTokenPayload struct {
	Token string `json:"token" form:"token"`
}

func (controller *authController) refreshUserToken(c *fiber.Ctx) error {
	payload := new(refreshUserTokenPayload)

	err := c.BodyParser(payload)
	if err != nil || payload.Token == "" {
		return c.SendStatus(fiber.StatusBadRequest)
	}

	ok, claims, err := validateToken(
		payload.Token,
		controller.config.UserTokenSecret,
	)
	if err != nil {
		log.Println("Failed to parse user token", err)
		return c.SendStatus(fiber.StatusInternalServerError)
	} else if !ok {
		log.Println("User token is invalid")
		return c.SendStatus(fiber.StatusBadRequest)
	}

	token, err := createToken(
		claims.Indentity,
		controller.config.UserTokenSecret,
		controller.config.UserTokenAud,
		controller.config.UserTokenExp,
	)
	if err != nil {
		log.Println("Failed to create new user token", err)
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.JSON(fiber.Map{
		"token": token,
	})
}
